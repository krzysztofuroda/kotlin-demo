package com.payzilch

data class Person(
    val name: String,
    val gender: Gender
)

enum class Gender {
    MALE, FEMALE
}

fun main() {
    val names = listOf(
        Person("Anna", Gender.FEMALE),
        Person("John", Gender.MALE),
        Person("Jane", Gender.FEMALE),
        Person("Robert", Gender.MALE)
    )

    names.females().forEach { println(it) }

    println("Voldemort".myOwnUpperCase())
    println("Voldemort".isShort)
}

private fun List<Person>.females(): List<Person> {
    return this.filter { it.gender == Gender.FEMALE }
}

fun String.myOwnUpperCase(): String {
    return "_${this.uppercase()}_"
}

val String.isShort: Boolean
    get() {
        return this.length < 5
    }
