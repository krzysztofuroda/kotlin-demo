package com.payzilch.ooo

import kotlin.random.Random

sealed class Shape {
    abstract fun area(): Double
}

class Rectangle(
    private val sizeX: Double,
    private val sizeY: Double
) : Shape() {
    override fun area() = sizeX * sizeY
}

class Circle(
    private val radius: Double
) : Shape() {
    override fun area(): Double {
        return Math.PI * radius * radius
    }
}

fun main() {

    // randomly return either Circle or Rectangle
    val shape: Shape = if (Random.nextInt(0, 2) == 0)
        Rectangle(10.0, 20.0)
    else
        Circle(10.0)

    handleShape(shape)
}

private fun handleShape(shape: Shape) = when (shape) {
    is Rectangle -> println("Rectangle area is " + shape.area())
    is Circle -> println("Circle area is " + shape.area())
}