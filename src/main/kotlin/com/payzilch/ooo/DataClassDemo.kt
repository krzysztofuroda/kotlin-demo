package com.payzilch.ooo

// generates:
// - equals()
// - hashCode()
// - toString()
// - copy()
data class Person2(
    val firstname: String,
    val lastname: String,
    val age: Int
) {
    // this does not go to the above methods
    val isUnderage = age < 18
}

fun main() {
    val john = Person2("John", "Smith", 30)

    println(john)
    with(john) {
        println(firstname)
        println(lastname)
        println(age)
        println(isUnderage)
    }

    val john2 = john.copy(age = john.age + 1)
    println(john2)

}