package com.payzilch.ooo

class CompanionObjectDemo {
    companion object {
        const val DEFAULT_VALUE: String = "MY_DEFAULT_VALUE"
    }

    fun doSomething() {
        println(DEFAULT_VALUE)
    }
}

fun main() {
    CompanionObjectDemo().doSomething()

    println(CompanionObjectDemo.DEFAULT_VALUE)
}