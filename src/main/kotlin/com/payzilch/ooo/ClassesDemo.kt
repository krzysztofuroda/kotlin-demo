package com.payzilch.ooo

class Entity(
    var id: Int,
    val val1: String,
    val val2: Long?
) {

    val val3: String //= val1 + val2

    init {
        // init block
        val3 = val1 + val2
    }

    constructor(entity: Entity) : this(entity.id, entity.val1, entity.val2) {
        // additional action here
    }

    constructor() : this(-1, "def", -1) {
        // additional action here
    }

    private fun privateFun() {

    }

}

interface DbRepository {
    fun findById(id: Int): Entity?
}

class Service(private val repository: DbRepository) {

    fun findObject(id: Int): Entity =
        repository.findById(id) ?: throw IllegalArgumentException("error!")
}

