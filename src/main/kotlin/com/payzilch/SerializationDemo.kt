package com.payzilch

import kotlinx.serialization.Serializable
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.encodeToJsonElement

@Serializable
data class PersonForSerialization(
    val name: String,
    val age: Int,
    val gender: Gender
)

fun main() {
    val person = PersonForSerialization("Krzysztof", 35, Gender.MALE)
    val json = Json.encodeToJsonElement(person)

    println(json)
}