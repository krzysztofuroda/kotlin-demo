package com.payzilch

fun main() {

    // COLLECTIONS - INITIALIZATION

    val namesList = listOf("John", "Megan", "Carolyn", "Rufus")
    val namesSet = setOf("Clint", "Bruce", "Clint")
    val emptySet = setOf<String>() // emptySet() / emptyList() / emptyMap()
    val listOldWay = ArrayList<String>()

    val mutableNames = mutableListOf("1", "2", "3")

    val ageMap = mapOf(
        "John" to 35,
        "Greg" to 18,
        "Rebecca" to 27
    )

    val ageMapDifferent = mapOf(
        Pair("John", 35),
        Pair("Dexter", 43)
    )

    // STREAMS
    println("filter -> " +
            namesList.filter { it.length > 4 }
    )

    println(
        "sequence -> " +
                namesList.asSequence().filter { it.length > 4 }.toList()
    )

    println("map -> " +
            namesList.map { it.uppercase() }
    )

    println(
        "random -> " +
                namesList.random()
    )

    println(
        "shuffle -> " +
                namesList.shuffled()
    )


}